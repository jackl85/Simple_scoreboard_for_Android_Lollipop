package test.neosperience.com.thelword;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
* Created by omar on 03/07/14.
*/
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private final RecyclerView mRecyclerView;
    private OnItemClick mOnItemClick;
    private OnItemEditClick mOnItemEditClick;
    private List<PeopleDummy> mPeoples;


    public OnItemClick getOnItemClick() {
        return mOnItemClick;
    }

    public void setOnItemClick(OnItemClick mOnItemClick) {
        this.mOnItemClick = mOnItemClick;
    }

    public OnItemEditClick getOnItemEditClick() {
        return mOnItemEditClick;
    }

    public void setOnItemEditClick(OnItemEditClick onItemEditClick) {
        mOnItemEditClick = onItemEditClick;
    }

    public List<PeopleDummy> getItems() {
        return mPeoples;
    }

    // Provide a reference to the type of views that you are using
    // (custom viewholder)
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View mRootView;
        public CardView mCard;
        public TextView mTextView;
        public EditText mEditTextView;
        public ImageView mIconView;
        public Button mButtonView;
        public ViewHolder(View v) {
            super(v);
            mRootView = v;
            mCard = (CardView) v.findViewById(R.id.card_view);
            mTextView = (TextView) v.findViewById(android.R.id.text1);
            mEditTextView = (EditText) v.findViewById(R.id.edit_text1);
            mIconView = (ImageView) v.findViewById(android.R.id.icon);
            mButtonView = (Button) v.findViewById(android.R.id.button1);

            // set the view's size, margins, paddings and layout parameters
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerAdapter(RecyclerView recyclerView) {
        mRecyclerView = recyclerView;
        mPeoples = new ArrayList<PeopleDummy>();
    }

    public void add(PeopleDummy peopleDummy) {
        addMulti(peopleDummy, true);
    }

    private void addMulti(PeopleDummy peopleDummy, boolean notifyDataSetChanged) {
        mPeoples.add(peopleDummy);
        if (notifyDataSetChanged) notifyDataSetChanged();
    }

    public void clear() {
        mPeoples.clear();
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_person, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(mPeoples.get(position).name);
        holder.mIconView.setImageResource(R.drawable.ic_user);

        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClick != null) mOnItemClick.onItemClick(view, holder, position);
            }
        });
        holder.mButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemEditClick != null) mOnItemEditClick.onItemEditClick(holder, position);
            }
        });
        holder.mCard.setElevation(10);
        switch (position){
            case 0:
                holder.mIconView.setColorFilter(PeopleDummy.PEOPLE_COLOR.BLUE.getColor());
                holder.mIconView.setTransitionName("p1");
                break;
            case 1:
                holder.mIconView.setColorFilter(PeopleDummy.PEOPLE_COLOR.GREEN.getColor());
                holder.mIconView.setTransitionName("p2");
                break;
            case 2:
                holder.mIconView.setColorFilter(PeopleDummy.PEOPLE_COLOR.YELLOW.getColor());
                holder.mIconView.setTransitionName("p3");
                break;
            case 3:
                holder.mIconView.setColorFilter(PeopleDummy.PEOPLE_COLOR.RED.getColor());
                holder.mIconView.setTransitionName("p4");
                break;
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mPeoples.size();
    }

    public static abstract class OnItemClick {
        abstract void onItemClick(View view, ViewHolder viewHolder, int position);
    }

    public static abstract class OnItemEditClick {
        abstract void onItemEditClick(ViewHolder viewHolder, int position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
